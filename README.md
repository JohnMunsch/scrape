# Scrape

Pulls entries from a set of blogs and displays links to each one along with an
accompanying image if one was available.

## Installation

If you're comfortable with Git, just grab this like any other project on Bitbucket or Github. If you're not, click the download link to the left (it looks like a cloud with an arrow raining down out of it).

If you don't already have Node.js and NPM installed, go to Nodejs.org. When you get there you'll see a big green "Install" button in the middle of the page to get what you need.

Once Node.js is installed, go to the directory for this project and type: `npm install`

That will install all the libraries this app is using.

To run the app type: `node scrape.js`

That's it. By default it starts up at port 1337 on your machine so you can get to it via this link here: [http://localhost:1337](http://localhost:1337)

## About The Code

Think simple. There's maybe 100 lines of HTML and around 200 lines of JavaScript in this thing. As it stands today you could print it out on a few pages of paper and see all of it (ignoring the list of sites it scrapes).

It's AngularJS on the front end with the Angular Material controls so it has a vaguely Google look to it.

On the back-end it's some pretty damn simple Node.js code.

If you want to modify or improve anything it should prove to be very simple.

## Using Scrape

It comes with a sample set of data pre-loaded but that may be days, weeks, or years old by the time you download this so you'll probably want to hit the refresh button in the UI to scrape the sites for fresh stuff. In the current version you'll see it grab a set of sites, listed only on the console from which you ran it, and after that is done you can refresh the page and see the new results. It doesn't add to the data it already had, the list it had before is gone.

You can use the controls at the bottom of the page to go to different pages or the left and right arrow keys on the keyboard to move forward and back a page.

The search in the upper right hand corner will allow you to quickly find a particular thing and you can click the name of a particular site below an item to filter to only that one site (or for a particular kind of content like magazines or comics).

To say that the code and the UI could both do with lots of improvements is a huge understatement. But it's still better than browsing these blogs from their respective sites.

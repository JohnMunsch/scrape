angular.module('Scrape')
  .controller('MainController', function ($scope, $location, $http, $window, filterFilter, hotkeys, allItems) {
    $scope.pageSize = 24;
    $scope.totalPages = 1;

    $scope.filtered = [];

    $scope.pageNumber = 1;
    $scope.search = '';
    $scope.sort = 'fingerprint';

    var searchOptions = $location.search();
    if (searchOptions) {
      console.log(searchOptions);
      if (searchOptions.pageNumber) {
        $scope.pageNumber = searchOptions.pageNumber;
      }

      if (searchOptions.search) {
        $scope.search = searchOptions.search;
      }

      if (searchOptions.sort) {
        $scope.sort = searchOptions.sort;
      }
    }

    // Add hotkeys for easy navigation.
    hotkeys.add({
      combo: 'right',
      description: 'Next Page',
      callback: function () {
        $scope.nextPage();
      }
    });
    hotkeys.add({
      combo: 'left',
      description: 'Prev Page',
      callback: function () {
        $scope.prevPage();
      }
    });

    $scope.refreshSites = function () {
      $http.get('/admin/refresh').success(function (results) {
        $window.location.reload();
      });
    };

    $scope.cancelSearch = function () {
      $scope.search = '';
    };

    // Pagination
    $scope.setPageNumber = function (pageNumber) {
      $scope.pageNumber = pageNumber;

      $location.search('pageNumber', pageNumber);
    };

    $scope.prevPage = function () {
      if ($scope.pageNumber > 1) {
        $scope.pageNumber--;
      }
    };

    $scope.nextPage = function () {
      if ($scope.pageNumber < $scope.totalPages) {
        $scope.pageNumber++;
      }
    };

    $scope.setSort = function (sort) {
      $scope.sort = sort;

      $location.search('sort', sort);
    };

    $scope.begin = function () {
      return ($scope.pageNumber - 1) * $scope.pageSize;
    };

    $scope.pageRange = function () {
      // Look at the current page. We want up to four pages prior to that page
      // (if they exist) and as many pages as are available after it (max 9).
      var firstPage = Math.max($scope.pageNumber - 4, 1);
      var lastPage = Math.min($scope.pageNumber + 10, $scope.totalPages + 1);

      return _.slice(_.range(firstPage, lastPage), 0, 10);
    };

    $scope.$watch('search', function (newValue, oldValue) {
      console.log('search', newValue);
      $location.search('search', newValue);

      // Create filtered
      if (newValue) {
        $scope.filtered = filterFilter(allItems, newValue);
      } else {
        $scope.filtered = allItems;
      }

      // Then calculate total pages.
      $scope.totalPages = Math.ceil($scope.filtered.length / $scope.pageSize);

      if ($scope.pageNumber > $scope.totalPages) {
        $scope.pageNumber = $scope.totalPages;
      }

      if ($scope.pageNumber < 1) {
        $scope.pageNumber = 1;
      }
    });
  });

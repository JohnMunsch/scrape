angular.module('Scrape').directive('taglist', function () {
  return {
    scope: {
      tags: '='
    },
    link: function (scope) {
      if (scope.tags) {
        var tagLinks = _.map(scope.tags, function (tag) {
          return '<a href="#/?search=' + tag + '">' + tag + '</a>';
        });

        scope.tagLinks = '(' + tagLinks.join('/') + ')';
      }
    },
    template: '<span ng-bind-html="tagLinks"></span>'
  };
});
